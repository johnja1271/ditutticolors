$(function () {
    $('[data-toggle="tooltip"]').tooltip()
  }),

  $('.carousel').carousel({
    interval: 2000
});

$('#exampleModal').on('show.bs.modal', function (e) {

    console.log('Inicio de apertura de modal');
    $('#btnModal').removeClass('btn-primary');
    $('#btnModal').addClass('btn-secondary');
    $('#btnModal').prop('disabled',true);

});

$('#exampleModal').on('shown.bs.modal', function (e) {

    console.log('Apertura de modal');
});

$('#exampleModal').on('hide.bs.modal', function (e) {

    console.log('Inicio de cierre de modal');
});

$('#exampleModal').on('hidden.bs.modal', function (e) {

    console.log('Cierre de modal');
    $('#btnModal').removeClass('btn-secondary');
    $('#btnModal').addClass('btn-primary');
    $('#btnModal').prop('disabled',false);
});